@setup
	
	$me = isset($me) ? $me : die("No user to connect to server specified (ex. --me=your_username)\n");
	
	$servers = [
		'app' => $me.'@app.wvnet.edu',
		'app01' => $me.'@app01.wvnet.edu',
		'beast' => $me.'@beast.wvnet.edu',
		'gambit' => $me.'@gambit.wvnet.edu',
		'gatekeeper' => $me.'@gatekeeper.wvnet.edu',
		'northstar' => $me.'@northstar.wvnet.edu',
		'oz' => $me.'@oz.wvnet.edu',
		'ozzy' => $me.'@ozzy.wvnet.edu',
		'parker' => $me.'@parker.wvnet.edu',
		'peter' => $me.'@peter.wvnet.edu',
		'phoenix' => $me.'@phoenix.wvnet.edu',
		'storm' => $me.'@storm.wvnet.edu',
		'vh' => $me.'@vh.wvnet.edu',
		'vhost01' => $me.'@vhost01.wvnet.edu',
		'vhost02' => $me.'@vhost02.wvnet.edu',
		'vhost03' => $me.'@vhost03.wvnet.edu',
		'vhost04' => $me.'@vhost04.wvnet.edu',
		'vhost05' => $me.'@vhost05.wvnet.edu',
		'vhost51' => $me.'@vhost51.wvnet.edu',
		'vhost99' => $me.'@vhost99.wvnet.edu',
		'webster' => $me.'@webster.wvnet.edu',
		'ws' => $me.'@ws.wvnet.edu',
	];

	$run_on = [
		'app',
		'app01',
		'beast',
		'gambit',
		'gatekeeper',
		'northstar',
		'oz',
		'ozzy',
		'parker',
		'peter',
		'phoenix',
		'storm',
		'vh',
		'vhost01',
		'vhost02',
		'vhost03',
		'vhost04',
		'vhost05',
		'vhost51',
		'vhost99',
		'webster',
		'ws',
	];


	$user = 'root';
	$password = isset($rootpass) ? $rootpass : null;

	$new_user = isset($dbuser) ? $dbuser : null;
	$new_password = isset($dbuserpass) ? $dbuserpass : 'test';
@endsetup

@servers($servers)

@task('test', ['on' => $run_on, 'parallel' => true])
	ls -la
@endtask

@task('createUser', ['on' => $run_on, 'parallel' => true])
	echo 'Connecting to database...'
	mysql --user="{{$user}}" --password="{{$password}}" --execute="CREATE USER '{{$new_user}}'@'localhost' IDENTIFIED BY '{{$new_password}}';"
	echo 'Added user: '.{{$new_user}}
@endtask

@task('updatePassword', ['on' => $run_on, 'parallel' => true])
	echo 'Connecting to database...'
	mysql --user="{{$user}}" --password="{{$password}}" --execute="SET PASSWORD FOR '{{$new_user}}'@'localhost' = PASSWORD('{{$new_password}}');"
	echo 'Updated password for '.{{$new_user}}.' to '.{{$new_password}}
@endtask

@task('grantPrivileges', ['on' => $run_on, 'parallel' => true])
	echo 'Granting priviledges...'
	mysql --user="{{$user}}" --password="{{$password}}" --execute="GRANT SELECT ON * . * TO '{{$new_user}}'@'localhost';"
	mysql --user="{{$user}}" --password="{{$password}}" --execute="GRANT LOCK TABLES ON * . * TO '{{$new_user}}'@'localhost';"
	echo 'Granted Read Only and Lock Tables Priviledges to '.{{$new_user}}
@endtask



