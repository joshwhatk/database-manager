## Database Manager

**Migrated to git.wvnet.edu**

This little application will log you in (--me=your_username) to all of media services servers with root password (--rootpass=password).

Some of the included tasks:

 - createUser, and
 - grantPrivileges

require an extra password (--dbuserpass=password) for the user (--dbuser=db_user_name) that is going to be created. If these are not included, the user `null` will be attempted to be created with the default password `test`.

All server connections will run instantly and simultaneously, try running `envoy run test --me=your_username` to test the script accross all of the servers.